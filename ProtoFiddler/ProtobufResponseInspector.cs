﻿using Fiddler;

namespace ProtoFiddler
{
    public class ProtobufResponseInspector : ProtobufInspectorBase, IResponseInspector2
    {
        protected override string GetHeader(Session fiddlerSession, string headerName)
        {
            return fiddlerSession.oResponse[headerName];
        }

        protected override byte[] GetBodyBytes(Session fiddlerSession)
        {
            return fiddlerSession.responseBodyBytes;
        }

        public HTTPResponseHeaders headers { get; set; }
    }
}
