﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace ProtoFiddler
{
    public partial class ProtobufInspectorControl : UserControl
    {
        private byte[] m_protobufBytes { get; set; }

        public ProtobufInspectorControl()
        {
            InitializeComponent();
        }

        public byte[] GetProtobufBytes()
        {
            txtLastError.Clear();

            byte[] protobuf = m_protobufBytes;

            if (!string.IsNullOrEmpty(txtProtoFile.Text) && !string.IsNullOrEmpty(cbType.SelectedItem.ToString()))
            {
                try
                {
                    return ProtobufWrap.EncodeWithProto(rtbProtobufData.Text, cbType.SelectedItem.ToString(), txtProtoFile.Text);
                }
                catch (Exception ex)
                {
                    txtLastError.Text = ex.Message;
                }
            }
            return protobuf;
        }

        public void SetProtobufBytes(byte[] protobufBytes)
        {
            rtbProtobufData.ReadOnly = true;
            rtbProtobufData.BackColor = Color.FromKnownColor(KnownColor.Control);
            DecodeGeneric(protobufBytes);
        }

        private void bnDecodeAs_Click(object sender, EventArgs e)
        {
            DecodeGeneric(m_protobufBytes, txtProtoFile.Text, cbType.SelectedItem.ToString());
        }

        private void DecodeGeneric(byte[] protobufBytes, string protoFile = "", string messageType = "")
        {
            m_protobufBytes = protobufBytes;
            rtbProtobufData.Clear();
            txtLastError.Clear();

            if (protobufBytes != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(protoFile) && !string.IsNullOrEmpty(messageType))
                    {
                        rtbProtobufData.Text = ProtobufWrap.DecodeWithProto(protobufBytes, protoFile, messageType);
                        rtbProtobufData.ReadOnly = false;
                        rtbProtobufData.BackColor = Color.White;
                    }
                    else
                    {
                        rtbProtobufData.Text = ProtobufWrap.DecodeRaw(protobufBytes);
                    }
                }
                catch (Exception ex)
                {
                    txtLastError.Text = ex.Message;
                }
            }
        }

        private void bnBrowse_Click(object sender, EventArgs e)
        {
            string lastOpenFile = Settings.Default.LastOpenFile;
            if (!string.IsNullOrEmpty(lastOpenFile))
            {
                openProtoFile.FileName = Path.GetFileName(lastOpenFile);
                openProtoFile.InitialDirectory = Path.GetDirectoryName(lastOpenFile);
            }

            if (DialogResult.OK == openProtoFile.ShowDialog())
            {
                if (File.Exists(openProtoFile.FileName))
                {
                    Settings.Default.LastOpenFile = openProtoFile.FileName;
                    Settings.Default.Save();
                    txtProtoFile.Text = openProtoFile.FileName;

                    string ns = string.Empty;
                    var nstok = File.ReadLines(txtProtoFile.Text).First().Split(" \t\n{},;".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    for (int x = 0; x < nstok.Length; ++x)
                    {
                        if (nstok[x] == "package")
                        {
                            ns = nstok[x + 1];
                            break;
                        }
                    }

                    cbType.Items.Clear();

                    string rawProtoFile = File.ReadAllText(txtProtoFile.Text);
                    string[] tokens = rawProtoFile.Split(" \t\n{},".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    for (int x = 0; x < tokens.Length; ++x)
                    {
                        if (tokens[x].CompareTo("message") == 0)
                        {
                            cbType.Items.Add(string.Format("{0}.{1}", ns, tokens[x + 1]));
                            cbType.Enabled = true;
                        }
                    }

                    cbType.SelectedIndex = 0;
                }
            }
        }
    }
}
