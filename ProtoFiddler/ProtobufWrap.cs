﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace ProtoFiddler
{
    internal struct ProtobufMessageBuffer
    {
        public int Size { get; set; }
        public int Offset { get; set; }
    }

    public static class ProtobufWrap
    {
        private static string GetFilePath(string fileName)
        {
            var fileInfo = new FileInfo(fileName);
            string f = fileInfo.Name;
            string filePath = fileInfo.FullName.Replace(f, string.Empty);
            return filePath;
        }

        private static Process StartProtocProcess(string args)
        {
            var procStartInfo = new ProcessStartInfo
            {
                FileName = "protoc",
                Arguments = args,
                RedirectStandardInput = true,
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
            };

            return Process.Start(procStartInfo);
        }

        private static ProtobufMessageBuffer GetBufferLength(byte[] protobuf)
        {
            int result = 0; 
            int n = -1;
            do
            {
                ++n;
                result |= (protobuf[n] & ~0x80) << (7 * n );
            } while ((protobuf[n] & 0x80) != 0);
            return new ProtobufMessageBuffer { Size = result, Offset = ++n };
        }

        private static ProtobufMessageBuffer PrepareBufferLength(byte[] protobuf)
        {
            var buf = new byte[4];
            var length = protobuf.Length;
            int offset = 0;
            unchecked
            {
                while (length > 0x7F)
                {
                    buf[offset++] = (byte)((byte)length & 0x7f | 0x80);
                    length >>= 7;
                }
                buf[offset++] = (byte)((byte)length & 0x7f);
            }
            return new ProtobufMessageBuffer { Size = BitConverter.ToInt32(buf, 0), Offset = offset };
        }

        private static byte[] AddProtobufLength(byte[] protobuf)
        {
            ProtobufMessageBuffer pmb = PrepareBufferLength(protobuf);
            var lengthBytes = BitConverter.GetBytes(pmb.Size);
            lengthBytes = lengthBytes.Take(pmb.Offset).ToArray();
            byte[] rv = new byte[lengthBytes.Length + protobuf.Length];
            Buffer.BlockCopy(lengthBytes, 0, rv, 0, lengthBytes.Length);
            Buffer.BlockCopy(protobuf, 0, rv, lengthBytes.Length, protobuf.Length);
            return rv;
        }

        private static string ReadProcessOutput(string args, byte[] protobuf)
        {
            string retVal = string.Empty;
            var proc = StartProtocProcess(args);
            var binaryWriter = new BinaryWriter(proc.StandardInput.BaseStream);
            binaryWriter.Write(protobuf);
            binaryWriter.Flush();
            binaryWriter.Close();

            retVal = proc.StandardOutput.ReadToEnd();

            proc.WaitForExit();

            if (proc.ExitCode != 0)
            {
                throw new Exception(proc.StandardError.ReadToEnd());
            }

            return retVal;
        }

        private static string DecodeGeneric(string args, byte[] protobuf)
        {
            string result = string.Empty;

            try
            {
                byte[] tempProtobuf = protobuf;

                do
                {
                    ProtobufMessageBuffer pmb = GetBufferLength(tempProtobuf);
                    result += ReadProcessOutput(args, tempProtobuf.Skip(pmb.Offset).Take(pmb.Size).ToArray());
                    tempProtobuf = tempProtobuf.Skip(pmb.Offset + pmb.Size).ToArray();
                } while (tempProtobuf.Length > 0);
            }
            catch (Exception ex)
            {
                try
                {
                    // Try decode without buffer size
                    result += ReadProcessOutput(args, protobuf);
                }
                catch (Exception exInner)
                {
                    throw new Exception(string.Format("1: {0} 2: {1}", ex.Message, exInner.Message));
                }
            }

            return result;
        }

        public static string DecodeRaw(byte[] protobuf)
        {
            return DecodeGeneric("--decode_raw", protobuf);
        }

        public static string DecodeWithProto(byte[] protobuf, string protoFile, string messageType)
        {
            string args = string.Format("--decode={0} --proto_path={1} {2}", messageType, GetFilePath(protoFile), protoFile);
            return DecodeGeneric(args, protobuf);
        }

        public static byte[] EncodeWithProto(string protobufMessage, string messageType, string protoFile)
        {
            var proc = StartProtocProcess(string.Format("--encode={0} --proto_path={1} {2}", messageType, GetFilePath(protoFile), protoFile));
            var streamWriter = new StreamWriter(proc.StandardInput.BaseStream);
            streamWriter.Write(protobufMessage);
            streamWriter.Flush();
            streamWriter.Close();

            var binaryReader = new BinaryReader(proc.StandardOutput.BaseStream);

            var buffer = binaryReader.ReadBytes(short.MaxValue);
            var str = Encoding.ASCII.GetString(buffer);

            proc.WaitForExit();
            if (proc.ExitCode != 0)
            {
                throw new Exception(proc.StandardError.ReadToEnd());
            }

            return AddProtobufLength(buffer);
        }
    }
}
