﻿using Fiddler;

namespace ProtoFiddler
{
    public class ProtobufRequestInspector : ProtobufInspectorBase, IRequestInspector2
    {
        protected override string GetHeader(Session fiddlerSession, string headerName)
        {
            return fiddlerSession.oRequest[headerName];
        }

        protected override byte[] GetBodyBytes(Session fiddlerSession)
        {
            return fiddlerSession.requestBodyBytes;
        }

        public HTTPRequestHeaders headers { get; set; }
    }
}
