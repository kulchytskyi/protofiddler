﻿namespace ProtoFiddler
{
    partial class ProtobufInspectorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openProtoFile = new System.Windows.Forms.OpenFileDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.bnDecodeAs = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.txtProtoFile = new System.Windows.Forms.TextBox();
            this.bnBrowse = new System.Windows.Forms.Button();
            this.rtbProtobufData = new System.Windows.Forms.RichTextBox();
            this.lblLastError = new System.Windows.Forms.Label();
            this.txtLastError = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openProtoFile
            // 
            this.openProtoFile.Filter = "Proto Files|*.proto";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.txtLastError);
            this.splitContainer1.Panel1.Controls.Add(this.lblLastError);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.bnDecodeAs);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.cbType);
            this.splitContainer1.Panel1.Controls.Add(this.txtProtoFile);
            this.splitContainer1.Panel1.Controls.Add(this.bnBrowse);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.rtbProtobufData);
            this.splitContainer1.Size = new System.Drawing.Size(849, 401);
            this.splitContainer1.SplitterDistance = 111;
            this.splitContainer1.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(15, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Protofile:";
            // 
            // bnDecodeAs
            // 
            this.bnDecodeAs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bnDecodeAs.Location = new System.Drawing.Point(715, 42);
            this.bnDecodeAs.Name = "bnDecodeAs";
            this.bnDecodeAs.Size = new System.Drawing.Size(84, 26);
            this.bnDecodeAs.TabIndex = 6;
            this.bnDecodeAs.Text = "Decode As";
            this.bnDecodeAs.UseVisualStyleBackColor = true;
            this.bnDecodeAs.Click += new System.EventHandler(this.bnDecodeAs_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(15, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "Select Type:";
            // 
            // cbType
            // 
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.Enabled = false;
            this.cbType.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbType.FormattingEnabled = true;
            this.cbType.Location = new System.Drawing.Point(133, 42);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(576, 26);
            this.cbType.Sorted = true;
            this.cbType.TabIndex = 5;
            // 
            // txtProtoFile
            // 
            this.txtProtoFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtProtoFile.Location = new System.Drawing.Point(133, 12);
            this.txtProtoFile.Name = "txtProtoFile";
            this.txtProtoFile.ReadOnly = true;
            this.txtProtoFile.Size = new System.Drawing.Size(576, 24);
            this.txtProtoFile.TabIndex = 2;
            // 
            // bnBrowse
            // 
            this.bnBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bnBrowse.Location = new System.Drawing.Point(715, 12);
            this.bnBrowse.Name = "bnBrowse";
            this.bnBrowse.Size = new System.Drawing.Size(84, 24);
            this.bnBrowse.TabIndex = 3;
            this.bnBrowse.Text = "Browse...";
            this.bnBrowse.UseVisualStyleBackColor = true;
            this.bnBrowse.Click += new System.EventHandler(this.bnBrowse_Click);
            // 
            // rtbProtobufData
            // 
            this.rtbProtobufData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbProtobufData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbProtobufData.Location = new System.Drawing.Point(0, 0);
            this.rtbProtobufData.Name = "rtbProtobufData";
            this.rtbProtobufData.ReadOnly = true;
            this.rtbProtobufData.Size = new System.Drawing.Size(849, 286);
            this.rtbProtobufData.TabIndex = 0;
            this.rtbProtobufData.Text = "";
            // 
            // lblLastError
            // 
            this.lblLastError.AutoSize = true;
            this.lblLastError.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLastError.Location = new System.Drawing.Point(15, 75);
            this.lblLastError.Name = "lblLastError";
            this.lblLastError.Size = new System.Drawing.Size(78, 18);
            this.lblLastError.TabIndex = 7;
            this.lblLastError.Text = "Last Error:";
            // 
            // txtLastError
            // 
            this.txtLastError.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtLastError.Location = new System.Drawing.Point(133, 75);
            this.txtLastError.Name = "txtLastError";
            this.txtLastError.ReadOnly = true;
            this.txtLastError.Size = new System.Drawing.Size(576, 24);
            this.txtLastError.TabIndex = 8;
            // 
            // ProtoBufInspectorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "ProtoBufInspectorControl";
            this.Size = new System.Drawing.Size(849, 401);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openProtoFile;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bnDecodeAs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.TextBox txtProtoFile;
        private System.Windows.Forms.Button bnBrowse;
        private System.Windows.Forms.RichTextBox rtbProtobufData;
        private System.Windows.Forms.TextBox txtLastError;
        private System.Windows.Forms.Label lblLastError;
    }
}
