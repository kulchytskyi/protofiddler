﻿using Fiddler;
using System.Windows.Forms;

[assembly: RequiredVersion("2.3.0.0")]

namespace ProtoFiddler
{
    public abstract class ProtobufInspectorBase : Inspector2
    {
        protected ProtobufInspectorControl Control { get; set; }

        protected abstract string GetHeader(Session fiddlerSession, string headerName);

        protected abstract byte[] GetBodyBytes(Session fiddlerSession);

        #region Inspector2

        public override void AssignSession(Session fiddlerSession)
        {
            var content = GetHeader(fiddlerSession, "Content-Type");
            Control.SetProtobufBytes(content.Contains("protobuf") ? GetBodyBytes(fiddlerSession) : null);
        }

        public override void AddToTab(TabPage tabPage)
        {
            Control = new ProtobufInspectorControl();
            tabPage.Text = "Protobuf";
            tabPage.Controls.Add(Control);
            tabPage.Controls[0].Dock = DockStyle.Fill;
        }

        public override int GetOrder()
        {
            return 0;
        }

        #endregion // Inspector2

        #region IBaseInspector2

        public void Clear()
        {
            Control.SetProtobufBytes(null);
        }

        public byte[] body
        {
            get { return Control.GetProtobufBytes(); }
            set { Control.SetProtobufBytes(value); }
        }

        public bool bDirty
        {
            get { return true; }
        }

        public bool bReadOnly { get; set; }

        #endregion // IBaseInspector2
    }
}
